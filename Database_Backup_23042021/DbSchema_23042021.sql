USE [PICO]
GO
/****** Object:  StoredProcedure [dbo].[SP_PurgeData]    Script Date: 4/23/2021 4:54:06 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_PurgeData]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SP_PurgeData]
GO
/****** Object:  StoredProcedure [dbo].[SP_ArchiveData]    Script Date: 4/23/2021 4:54:06 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_ArchiveData]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SP_ArchiveData]
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__ValidateP__Archi__1273C1CD]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[ValidatePaymentArchive] DROP CONSTRAINT [DF__ValidateP__Archi__1273C1CD]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__ValidateP__IsSea__108B795B]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[ValidatePayment] DROP CONSTRAINT [DF__ValidateP__IsSea__108B795B]
END

GO
/****** Object:  Table [dbo].[ValidatePaymentArchive]    Script Date: 4/23/2021 4:54:06 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ValidatePaymentArchive]') AND type in (N'U'))
DROP TABLE [dbo].[ValidatePaymentArchive]
GO
/****** Object:  Table [dbo].[ValidatePayment]    Script Date: 4/23/2021 4:54:06 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ValidatePayment]') AND type in (N'U'))
DROP TABLE [dbo].[ValidatePayment]
GO
USE [master]
GO
/****** Object:  Database [PICO]    Script Date: 4/23/2021 4:54:06 PM ******/
IF  EXISTS (SELECT name FROM sys.databases WHERE name = N'PICO')
DROP DATABASE [PICO]
GO
/****** Object:  Database [PICO]    Script Date: 4/23/2021 4:54:06 PM ******/
IF NOT EXISTS (SELECT name FROM sys.databases WHERE name = N'PICO')
BEGIN
CREATE DATABASE [PICO]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'PICO', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.MSSQLSERVER\MSSQL\DATA\PICO.mdf' , SIZE = 5120KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'PICO_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.MSSQLSERVER\MSSQL\DATA\PICO_log.ldf' , SIZE = 5120KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
END

GO
ALTER DATABASE [PICO] SET COMPATIBILITY_LEVEL = 120
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [PICO].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [PICO] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [PICO] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [PICO] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [PICO] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [PICO] SET ARITHABORT OFF 
GO
ALTER DATABASE [PICO] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [PICO] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [PICO] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [PICO] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [PICO] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [PICO] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [PICO] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [PICO] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [PICO] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [PICO] SET  DISABLE_BROKER 
GO
ALTER DATABASE [PICO] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [PICO] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [PICO] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [PICO] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [PICO] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [PICO] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [PICO] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [PICO] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [PICO] SET  MULTI_USER 
GO
ALTER DATABASE [PICO] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [PICO] SET DB_CHAINING OFF 
GO
ALTER DATABASE [PICO] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [PICO] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
ALTER DATABASE [PICO] SET DELAYED_DURABILITY = DISABLED 
GO
USE [PICO]
GO
/****** Object:  Table [dbo].[ValidatePayment]    Script Date: 4/23/2021 4:54:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ValidatePayment]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ValidatePayment](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[TravelId] [int] NOT NULL,
	[OrderId] [int] NOT NULL,
	[CustomerKey] [varchar](250) NOT NULL,
	[InitiateTimestamp] [datetime] NOT NULL,
	[ValidatePaymentStatus] [varchar](50) NOT NULL,
	[ValidationPerformedBy] [varchar](50) NULL,
	[CreatedOn] [datetime] NOT NULL,
	[CreatedBy] [varchar](250) NOT NULL,
	[ModifiedOn] [datetime] NOT NULL,
	[ModifiedBy] [varchar](250) NOT NULL,
	[IsSeason] [bit] NULL,
	[FirstName] [varchar](50) NULL,
	[LastName] [varchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ValidatePaymentArchive]    Script Date: 4/23/2021 4:54:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ValidatePaymentArchive]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ValidatePaymentArchive](
	[Id] [int] NOT NULL,
	[TravelId] [int] NOT NULL,
	[OrderId] [int] NOT NULL,
	[CustomerKey] [varchar](250) NOT NULL,
	[InitiateTimestamp] [datetime] NOT NULL,
	[ValidatePaymentStatus] [varchar](50) NOT NULL,
	[ValidationPerformedBy] [varchar](50) NULL,
	[CreatedOn] [datetime] NOT NULL,
	[CreatedBy] [varchar](250) NOT NULL,
	[ModifiedOn] [datetime] NOT NULL,
	[ModifiedBy] [varchar](250) NOT NULL,
	[IsSeason] [bit] NULL,
	[FirstName] [varchar](50) NULL,
	[LastName] [varchar](50) NULL,
	[ArchiveTime] [datetime] NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__ValidateP__IsSea__108B795B]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[ValidatePayment] ADD  DEFAULT ('FALSE') FOR [IsSeason]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__ValidateP__Archi__1273C1CD]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[ValidatePaymentArchive] ADD  DEFAULT (getutcdate()) FOR [ArchiveTime]
END

GO
/****** Object:  StoredProcedure [dbo].[SP_ArchiveData]    Script Date: 4/23/2021 4:54:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_ArchiveData]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[SP_ArchiveData] AS' 
END
GO



ALTER PROCEDURE [dbo].[SP_ArchiveData]
@OldDataDays INT
AS
BEGIN 
SET NOCOUNT ON
SET XACT_ABORT ON
BEGIN TRY
BEGIN TRANSACTION

DECLARE @IDToBeMoved TABLE (Id INT);
DECLARE @OldDate DATETIME, @rowCount INT;

SET @OldDate = DATEADD(day,-@OldDataDays,CONVERT(date, getdate()));
SET @rowCount =0;

INSERT INTO @IDToBeMoved
SELECT ID FROM [dbo].[ValidatePayment] WHERE CONVERT(date,CreatedOn) <= @OldDate AND [ValidatePaymentStatus] IN ('COMPLETED','FAILED')

SET @rowCount = (SELECT COUNT(*) FROM @IDToBeMoved);

--ARCHIVE  DATA
IF @rowCount > 0
BEGIN

INSERT INTO [dbo].[ValidatePaymentArchive] 
([Id],[TravelId],[OrderId],[CustomerKey],[InitiateTimestamp],[ValidatePaymentStatus],[ValidationPerformedBy],[CreatedOn],[CreatedBy],[ModifiedOn],[ModifiedBy],[IsSeason],[FirstName],[LastName],[ArchiveTime])
SELECT [Id],[TravelId],[OrderId],[CustomerKey],[InitiateTimestamp],[ValidatePaymentStatus],[ValidationPerformedBy],[CreatedOn],[CreatedBy],[ModifiedOn],[ModifiedBy],[IsSeason],[FirstName],[LastName],
GETUTCDATE() FROM [dbo].[ValidatePayment] WHERE [Id] IN (SELECT Id FROM @IDToBeMoved);
--DELETE DATA FROM [dbo].[ValidatePayment]
DELETE FROM  [dbo].[ValidatePayment] WHERE  [Id] IN (SELECT Id FROM @IDToBeMoved);
DELETE FROM @IDToBeMoved;

END



COMMIT

END TRY

BEGIN CATCH 
ROLLBACK TRANSACTION; 
END CATCH 

END



GO
/****** Object:  StoredProcedure [dbo].[SP_PurgeData]    Script Date: 4/23/2021 4:54:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_PurgeData]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[SP_PurgeData] AS' 
END
GO



ALTER PROCEDURE [dbo].[SP_PurgeData]
@OldDataDays INT
AS
BEGIN 
SET NOCOUNT ON
SET XACT_ABORT ON
BEGIN TRY
BEGIN TRANSACTION

DECLARE @IDToBeMoved TABLE (Id INT);
DECLARE @OldDate DATETIME, @rowCount INT;

SET @OldDate = DATEADD(day,-@OldDataDays,CONVERT(date, getdate()));
SET @rowCount =0;

INSERT INTO @IDToBeMoved
SELECT ID FROM [dbo].[ValidatePaymentArchive] WHERE CreatedOn < @OldDate

SET @rowCount = (SELECT COUNT(*) FROM @IDToBeMoved);

--DELETE  DATA
IF @rowCount > 0
BEGIN
DELETE FROM [dbo].[ValidatePaymentArchive] WHERE  [Id] IN (SELECT Id FROM @IDToBeMoved);
DELETE FROM @IDToBeMoved;
END

COMMIT

END TRY

BEGIN CATCH 
ROLLBACK TRANSACTION; 
END CATCH 

END



GO
USE [master]
GO
ALTER DATABASE [PICO] SET  READ_WRITE 
GO
