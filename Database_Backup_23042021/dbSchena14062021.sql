USE [PICO]
GO
/****** Object:  StoredProcedure [dbo].[SP_PurgeData]    Script Date: 6/14/2021 10:27:31 AM ******/
DROP PROCEDURE IF EXISTS [dbo].[SP_PurgeData]
GO
/****** Object:  StoredProcedure [dbo].[SP_ArchiveData]    Script Date: 6/14/2021 10:27:31 AM ******/
DROP PROCEDURE IF EXISTS [dbo].[SP_ArchiveData]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ValidatePaymentArchive]') AND type in (N'U'))
ALTER TABLE [dbo].[ValidatePaymentArchive] DROP CONSTRAINT IF EXISTS [DF__ValidateP__Archi__239E4DCF]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ValidatePayment]') AND type in (N'U'))
ALTER TABLE [dbo].[ValidatePayment] DROP CONSTRAINT IF EXISTS [DF__ValidateP__IsSea__267ABA7A]
GO
/****** Object:  Table [dbo].[ValidatePaymentArchive]    Script Date: 6/14/2021 10:27:31 AM ******/
DROP TABLE IF EXISTS [dbo].[ValidatePaymentArchive]
GO
/****** Object:  Table [dbo].[ValidatePayment]    Script Date: 6/14/2021 10:27:31 AM ******/
DROP TABLE IF EXISTS [dbo].[ValidatePayment]
GO
/****** Object:  Table [dbo].[ValidatePayment]    Script Date: 6/14/2021 10:27:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ValidatePayment](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[TravelId] [int] NOT NULL,
	[OrderId] [int] NOT NULL,
	[CustomerKey] [varchar](250) NOT NULL,
	[InitiateTimestamp] [datetime] NOT NULL,
	[ValidatePaymentStatus] [varchar](50) NOT NULL,
	[ValidationPerformedBy] [varchar](50) NULL,
	[CreatedOn] [datetime] NOT NULL,
	[CreatedBy] [varchar](250) NOT NULL,
	[ModifiedOn] [datetime] NOT NULL,
	[ModifiedBy] [varchar](250) NOT NULL,
	[IsSeason] [bit] NULL,
	[FirstName] [varchar](50) NULL,
	[LastName] [varchar](50) NULL,
	[FailedAPI] [varchar](50) NULL,
	[ErrorDetail] [varchar](500) NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ValidatePaymentArchive]    Script Date: 6/14/2021 10:27:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ValidatePaymentArchive](
	[Id] [int] NOT NULL,
	[TravelId] [int] NOT NULL,
	[OrderId] [int] NOT NULL,
	[CustomerKey] [varchar](250) NOT NULL,
	[InitiateTimestamp] [datetime] NOT NULL,
	[ValidatePaymentStatus] [varchar](50) NOT NULL,
	[ValidationPerformedBy] [varchar](50) NULL,
	[CreatedOn] [datetime] NOT NULL,
	[CreatedBy] [varchar](250) NOT NULL,
	[ModifiedOn] [datetime] NOT NULL,
	[ModifiedBy] [varchar](250) NOT NULL,
	[IsSeason] [bit] NULL,
	[FirstName] [varchar](50) NULL,
	[LastName] [varchar](50) NULL,
	[ArchiveTime] [datetime] NULL,
	[FailedAPI] [varchar](50) NULL,
	[ErrorDetail] [varchar](500) NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ValidatePayment] ADD  DEFAULT ('FALSE') FOR [IsSeason]
GO
ALTER TABLE [dbo].[ValidatePaymentArchive] ADD  DEFAULT (getutcdate()) FOR [ArchiveTime]
GO
/****** Object:  StoredProcedure [dbo].[SP_ArchiveData]    Script Date: 6/14/2021 10:27:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[SP_ArchiveData]
@OldDataDays INT
AS
BEGIN 
SET NOCOUNT ON
SET XACT_ABORT ON
BEGIN TRY
BEGIN TRANSACTION

DECLARE @IDToBeMoved TABLE (Id INT);
DECLARE @OldDate DATETIME, @rowCount INT;

SET @OldDate = DATEADD(day,-@OldDataDays,CONVERT(date, getdate()));
SET @rowCount =0;

INSERT INTO @IDToBeMoved
SELECT ID FROM [dbo].[ValidatePayment] WHERE CONVERT(date,CreatedOn) <= @OldDate AND [ValidatePaymentStatus] IN ('COMPLETED','FAILED')

SET @rowCount = (SELECT COUNT(*) FROM @IDToBeMoved);

--ARCHIVE  DATA
IF @rowCount > 0
BEGIN

INSERT INTO [dbo].[ValidatePaymentArchive] 
([Id],[TravelId],[OrderId],[CustomerKey],[InitiateTimestamp],[ValidatePaymentStatus],[ValidationPerformedBy],[CreatedOn],[CreatedBy],[ModifiedOn],[ModifiedBy],[IsSeason],[FirstName],[LastName],[FailedAPI],[ErrorDetail],[ArchiveTime])
SELECT [Id],[TravelId],[OrderId],[CustomerKey],[InitiateTimestamp],[ValidatePaymentStatus],[ValidationPerformedBy],[CreatedOn],[CreatedBy],[ModifiedOn],[ModifiedBy],[IsSeason],[FirstName],[LastName],[FailedAPI],[ErrorDetail],
GETUTCDATE() FROM [dbo].[ValidatePayment] WHERE [Id] IN (SELECT Id FROM @IDToBeMoved);
--DELETE DATA FROM [dbo].[ValidatePayment]
DELETE FROM  [dbo].[ValidatePayment] WHERE  [Id] IN (SELECT Id FROM @IDToBeMoved);
DELETE FROM @IDToBeMoved;

END



COMMIT

END TRY

BEGIN CATCH 
ROLLBACK TRANSACTION; 
END CATCH 

END


GO
/****** Object:  StoredProcedure [dbo].[SP_PurgeData]    Script Date: 6/14/2021 10:27:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[SP_PurgeData]
@OldDataDays INT
AS
BEGIN 
SET NOCOUNT ON
SET XACT_ABORT ON
BEGIN TRY
BEGIN TRANSACTION

DECLARE @IDToBeMoved TABLE (Id INT);
DECLARE @OldDate DATETIME, @rowCount INT;

SET @OldDate = DATEADD(day,-@OldDataDays,CONVERT(date, getdate()));
SET @rowCount =0;

INSERT INTO @IDToBeMoved
SELECT ID FROM [dbo].[ValidatePaymentArchive] WHERE CreatedOn < @OldDate

SET @rowCount = (SELECT COUNT(*) FROM @IDToBeMoved);

--DELETE  DATA
IF @rowCount > 0
BEGIN
DELETE FROM [dbo].[ValidatePaymentArchive] WHERE  [Id] IN (SELECT Id FROM @IDToBeMoved);
DELETE FROM @IDToBeMoved;
END

COMMIT

END TRY

BEGIN CATCH 
ROLLBACK TRANSACTION; 
END CATCH 

END


GO
